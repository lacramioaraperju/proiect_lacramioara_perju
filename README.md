## Wantsome - Project Budget Tracker

### 1. Description

This is an application which allows a user to track personal budget.

Supported actions:
 - add/edit/delete transactions
 - add/edit/delete categories
 - add/edit/delete expense targets
 - view all transactions, only for a category or for a category type
 - sort the transactions by the id, date or amount
 - view all categories or only one type
 - sort the categories by the id
 - view all expense targets, only for a category or only for one state
 - sort the expense targets by the id, start date or amount
 - show graph expense targets about status
 - reports page with some statistics and graphs for a period

---
### 2. Setup

No setup needed, just start the application. If the database is missing
(like on first startup), it will create a new database (of type SQLite,
stored in a local file named 'budgetTracker.db'), and use it to save the future data.

Once the web app starts, navigate with a web browser at url: <http://localhost:4567>

---
### 3. Technical details

__User interface__

The project includes a user interface:
- web app (started with Main class)

__Technologies__

- main code is written in Java (minimum version: 8)
- it uses a small embedded database of type SQLite, using SQL and JDBC to
  connect the Java code to it
- it uses Spark micro web framework (which includes an embedded web server, Jetty)
- web pages: using the Velocity templating engine, to separate the UI code 
  from Java code; UI code consists of basic HTML and CSS code (and a little Javascript),
  using Bootstrap framework
- charts: using JavaScript library Google Charts (https://developers.google.com/chart)
  
- includes some unit tests for DB part (using JUnit library)

__Code structure__

- java code is organized in packages by its role, on layers:
  - db - database part, including DTOs and DAOs, as well as the code to init
    and connect to the db
  - ui - code related to the interface/presentation layer

- web resources are found in `main/resources` folder:
  - under `/public` folder - static resources to be served by the web server
    directly (images, css files)
  - all other (directly under `/resources`) - the Velocity templates
  
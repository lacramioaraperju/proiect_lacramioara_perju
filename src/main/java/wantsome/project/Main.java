package wantsome.project;

import wantsome.project.db.service.DbInitService;
import wantsome.project.web.*;

import static spark.Spark.*;

/**
 * Main class of the application. Using Spark framework.
 */
public class Main {

    public static void main(String[] args) {
        setup();
        startWebServer();
    }

    private static void setup() {
        DbInitService.initDatabase();
        DbInitService.addSampleTransactions();
        DbInitService.addSampleExpenseTargets();
    }

    private static void startWebServer() {
        staticFileLocation("public");

        redirect.get("/", "/transactions");

        get("/transactions", TransactionsPageController::showTransactionsPage);
        get("/categories", CategoriesPageController::showCategoriesPage);
        get("/expenseTargets", ExpenseTargetsPageController::showExpenseTargetsPage);
        get("/reports", ReportsPageController::showReportsPage);


        get("/category/add", AddEditCategoryPageController::showAddForm);
        post("/category/add", AddEditCategoryPageController::handleAddEditRequest);

        get("/category/edit", AddEditCategoryPageController::showEditForm);
        post("/category/edit", AddEditCategoryPageController::handleAddEditRequest);


        get("/transaction/add", AddEditTransactionPageController::showAddForm);
        post("/transaction/add", AddEditTransactionPageController::handleAddEditRequest);

        get("/transaction/edit", AddEditTransactionPageController::showEditForm);
        post("/transaction/edit", AddEditTransactionPageController::handleAddEditRequest);


        get("/expenseTarget/add", AddEditExpenseTargetPageController::showAddForm);
        post("/expenseTarget/add", AddEditExpenseTargetPageController::handleAddEditRequest);

        get("/expenseTarget/edit", AddEditExpenseTargetPageController::showEditForm);
        post("/expenseTarget/edit", AddEditExpenseTargetPageController::handleAddEditRequest);



        get("/transaction/delete", TransactionsPageController::handleDeleteRequest);
        get("/category/delete", CategoriesPageController::handleDeleteRequest);
        get("/expenseTarget/delete", ExpenseTargetsPageController::handleDeleteRequest);


        get("/transaction/deleteAll", TransactionsPageController::handleDeleteAllRequest);
        get("/category/deleteAll", CategoriesPageController::handleDeleteAllRequest);
        get("/expenseTarget/deleteAll", ExpenseTargetsPageController::handleDeleteAllRequest);


        exception(Exception.class, ErrorPageController::handleException);

        awaitInitialization();
        System.out.println("\nServer started: http://localhost:4567/");
    }
}
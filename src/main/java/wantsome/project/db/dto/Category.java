package wantsome.project.db.dto;

import java.util.Objects;

public class Category {

    private final int id;
    private final String name;
    private final Type type;

    public Category(String name, Type type) {
        this(0, name, type);
    }

    public Category(int id, String name, Type type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return id == category.id &&
                Objects.equals(name, category.name) &&
                type == category.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, type);
    }
}
package wantsome.project.db.dto;

import java.sql.Date;
import java.util.Objects;

public class ExpenseTarget {

    private final int id;
    private final int categoryId;
    private final Date startDate;
    private final Date endDate;
    private final double amount;
    private final State state;

    public ExpenseTarget(int categoryId, Date startDate, Date endDate, double amount, State state) {
        this(0, categoryId, startDate, endDate, amount, state);
    }

    public ExpenseTarget(int id, int categoryId, Date startDate, Date endDate, double amount, State state) {
        this.id = id;
        this.categoryId = categoryId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.amount = amount;
        this.state = state;
    }

    public int getId() {
        return id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public double getAmount() {
        return amount;
    }

    public State getState() {
        return state;
    }

    @Override
    public String toString() {
        return "ExpenseTarget{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", amount=" + amount +
                ", state=" + state +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExpenseTarget that = (ExpenseTarget) o;
        return id == that.id &&
                categoryId == that.categoryId &&
                Double.compare(that.amount, amount) == 0 &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                state == that.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, categoryId, startDate, endDate, amount, state);
    }
}

package wantsome.project.db.dto;

import java.sql.Date;
import java.util.Objects;

public class ExpenseTargetFull {

    private final int id;
    private final int categoryId;
    private final String categoryName;
    private final Date startDate;
    private final Date endDate;
    private final double amount;
    private final State state;

    public ExpenseTargetFull(int id, int categoryId, String categoryName, Date startDate, Date endDate, double amount, State state) {
        this.id = id;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.amount = amount;
        this.state = state;
    }

    public ExpenseTargetFull(int categoryId, String categoryName, Date startDate, Date endDate, double amount, State state) {
        this(0, categoryId,  categoryName, startDate, endDate, amount, state);
    }

    public int getId() {
        return id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public double getAmount() {
        return amount;
    }

    public State getState() {
        return state;
    }

    @Override
    public String toString() {
        return "ExpenseTargetFull{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", amount=" + amount +
                ", state=" + state +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExpenseTargetFull that = (ExpenseTargetFull) o;
        return id == that.id &&
                categoryId == that.categoryId &&
                Double.compare(that.amount, amount) == 0 &&
                Objects.equals(categoryName, that.categoryName) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                state == that.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, categoryId, categoryName, startDate, endDate, amount, state);
    }
}

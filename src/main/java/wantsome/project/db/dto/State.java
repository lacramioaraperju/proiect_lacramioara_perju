package wantsome.project.db.dto;

public enum State {

    ACTIVE("Active"),
    COMPLETED("Completed"),
    UNCOMPLETED("Uncompleted");

    private final String label;

    State(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}

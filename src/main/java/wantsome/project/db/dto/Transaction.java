package wantsome.project.db.dto;

import java.sql.Date;
import java.util.Objects;

public class Transaction {

    private final int id;
    private final int categoryId;
    private final Date date;
    private final String details;
    private final double amount;

    public Transaction(int categoryId, Date date, double amount) {
        this(0, categoryId, date, "", amount);
    }

    public Transaction(int categoryId, Date date, String details, double amount) {
        this(0, categoryId, date, details, amount);
    }

    public Transaction(int id, int categoryId, Date date, String details, double amount) {
        this.id = id;
        this.categoryId = categoryId;
        this.date = date;
        this.details = details;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public Date getDate() {
        return date;
    }

    public String getDetails() {
        return details;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", date=" + date +
                ", details='" + details + '\'' +
                ", amount=" + amount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return id == that.id &&
                categoryId == that.categoryId &&
                Double.compare(that.amount, amount) == 0 &&
                Objects.equals(date, that.date) &&
                Objects.equals(details, that.details);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, categoryId, date, details, amount);
    }
}

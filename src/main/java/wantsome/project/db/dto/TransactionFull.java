package wantsome.project.db.dto;

import java.sql.Date;
import java.util.Objects;

public class TransactionFull {

    private final int id;
    private final int categoryId;
    private final String categoryName;
    private final Type categoryType;
    private final Date date;
    private final String details;
    private final double amount;

    public TransactionFull(int id, int categoryId, String categoryName, Type categoryType, Date date, String details, double amount) {
        this.id = id;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.categoryType = categoryType;
        this.date = date;
        this.details = details;
        this.amount = amount;
    }

    public TransactionFull(int categoryId, String categoryName, Type categoryType, Date date, String details, double amount) {
        this(0, categoryId, categoryName, categoryType, date, details, amount);
    }

    public TransactionFull(int categoryId, String categoryName, Type categoryType, Date date, double amount) {
        this( categoryId, categoryName, categoryType, date, "", amount);
    }

    public int getId() {
        return id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public Type getCategoryType() {
        return categoryType;
    }

    public Date getDate() {
        return date;
    }

    public String getDetails() {
        return details;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "TransactionFull{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", categoryType=" + categoryType +
                ", date=" + date +
                ", details='" + details + '\'' +
                ", amount=" + amount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionFull that = (TransactionFull) o;
        return id == that.id &&
                categoryId == that.categoryId &&
                Double.compare(that.amount, amount) == 0 &&
                Objects.equals(categoryName, that.categoryName) &&
                categoryType == that.categoryType &&
                Objects.equals(date, that.date) &&
                Objects.equals(details, that.details);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, categoryId, categoryName, categoryType, date, details, amount);
    }
}

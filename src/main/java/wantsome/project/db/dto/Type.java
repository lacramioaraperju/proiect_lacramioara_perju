package wantsome.project.db.dto;

public enum Type {

    EXPENSE("Expense"),
    INCOME("Income");

    private final String label;

    Type(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}

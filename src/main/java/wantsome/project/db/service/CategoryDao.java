package wantsome.project.db.service;

import wantsome.project.db.DbManager;
import wantsome.project.db.dto.Category;
import wantsome.project.db.dto.Type;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CategoryDao {

    public static List<Category> loadAll() {

        String sql = "SELECT * FROM CATEGORIES";

        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery(sql)) {

            List<Category> categoriesList = new ArrayList<>();
            while (rs.next()) {
                categoriesList.add(extractCategoryFromResult(rs));
            }
            return categoriesList;

        } catch (SQLException e) {
            throw new RuntimeException("Error loading categories: " + e.getMessage());
        }
    }

    private static Category extractCategoryFromResult(ResultSet rs) throws SQLException {
        return new Category(
                rs.getInt("ID"),
                rs.getString("NAME"),
                Type.valueOf(rs.getString("TYPE")));
    }

    public static Optional<Category> load(int id) {

        String sql = "SELECT * FROM CATEGORIES WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(extractCategoryFromResult(rs));
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading category " + id + ": " + e.getMessage());
        }
    }

    public static void insert(Category category) {

        String sql = "INSERT INTO CATEGORIES (NAME, TYPE) VALUES (?, ?)";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, category.getName());
            ps.setString(2, category.getType().toString());

            ps.execute();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed: CATEGORIES.NAME, CATEGORIES.TYPE") ?
                    "a category with same name and type already exists" :
                    e.getMessage();
            throw new RuntimeException("Error saving new category: " + details);

        }
    }

    public static void update(Category category) {
        String sql = "UPDATE CATEGORIES " +
                "SET NAME = ?, " +
                "TYPE = ? " +
                "WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, category.getName());
            ps.setString(2, category.getType().toString());
            ps.setInt(3, category.getId());

            ps.executeUpdate();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed: CATEGORIES.NAME, CATEGORIES.TYPE") ?
                    "a category with same name and type already exists" :
                    e.getMessage();
            throw new RuntimeException("Error updating category " + category.getId() + ": " + details);
        }
    }

    public static void delete(int id) {
        String sql = "DELETE FROM CATEGORIES WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, id);

            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error deleting category " + id + ": " + e.getMessage());
        }
    }

    public static void deleteAll() {
        String sql = "DELETE FROM CATEGORIES";

        try (Connection conn = DbManager.getConnection();
             Statement ps = conn.createStatement()) {

            ps.execute(sql);

        } catch (SQLException e) {
            throw new RuntimeException("Error deleting all categories : " + e.getMessage());
        }
    }
}

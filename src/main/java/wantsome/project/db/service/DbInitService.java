package wantsome.project.db.service;

import wantsome.project.db.dto.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;

import static wantsome.project.db.DbManager.getConnection;

public class DbInitService {

    private static final String CREATE_CATEGORIES_SQL =
            "CREATE TABLE IF NOT EXISTS CATEGORIES ( " +
                    "ID INTEGER NOT NULL PRIMARY KEY ," +
                    "NAME TEXT NOT NULL ," +
                    "TYPE TEXT NOT NULL CHECK ( TYPE IN ('" + Type.INCOME + "', '" + Type.EXPENSE + "'))," +
                    "CONSTRAINT NAME_TYPE_UQ UNIQUE (NAME, TYPE)" +
                    ")";

    private static final String CREATE_TRANSACTIONS_SQL =
            "CREATE TABLE IF NOT EXISTS TRANSACTIONS ( " +
                    "ID INTEGER NOT NULL PRIMARY KEY," +
                    "CATEGORY_ID INTEGER NOT NULL REFERENCES CATEGORIES(ID) ON DELETE CASCADE ON UPDATE CASCADE," +
                    "TRANSACTION_DATE DATE NOT NULL," +
                    "DETAILS TEXT," +
                    "AMOUNT REAL NOT NULL" +
                    ")";

    private static final String CREATE_EXPENSE_TARGETS_SQL =
            "CREATE TABLE IF NOT EXISTS EXPENSE_TARGETS ( " +
                    "ID INTEGER NOT NULL PRIMARY KEY," +
                    "CATEGORY_ID INTEGER NOT NULL REFERENCES CATEGORIES(ID) ON DELETE CASCADE ON UPDATE CASCADE," +
                    "START_DATE DATE NOT NULL," +
                    "END_DATE DATE NOT NULL," +
                    "AMOUNT REAL NOT NULL," +
                    "STATE TEXT NOT NULL CHECK ( STATE IN ('" + State.ACTIVE + "', '" + State.COMPLETED + "', '" + State.UNCOMPLETED + "'))" +
                    ")";


    public static void initDatabase() {
        createMissingTables();
        createDefaultCategories();
    }

    private static void createMissingTables() {
        try (Connection conn = getConnection();
             Statement st = conn.createStatement()) {

            st.execute(CREATE_CATEGORIES_SQL);
            st.execute(CREATE_TRANSACTIONS_SQL);
            st.execute(CREATE_EXPENSE_TARGETS_SQL);

        } catch (SQLException e) {
            throw new RuntimeException("Error creating missing tables: " + e.getMessage());
        }
    }

    private static void createDefaultCategories() {
        if (CategoryDao.loadAll().isEmpty()) {
            CategoryDao.insert(new Category("food", Type.EXPENSE));
            CategoryDao.insert(new Category("salary", Type.INCOME));
            CategoryDao.insert(new Category("rent", Type.EXPENSE));
            CategoryDao.insert(new Category("utility bills", Type.EXPENSE));
            CategoryDao.insert(new Category("other", Type.EXPENSE));
            CategoryDao.insert(new Category("other", Type.INCOME));
        }
    }

    public static void addSampleTransactions() {
        if (TransactionDao.loadAll().isEmpty()) {
            TransactionDao.insert(new Transaction(1, Date.valueOf("2020-02-21"), "dinner", 12.5));
            TransactionDao.insert(new Transaction(4, Date.valueOf("2020-01-23"), "water", 40.8));
            TransactionDao.insert(new Transaction(2, Date.valueOf("2019-12-24"), "salary", 2000));
            TransactionDao.insert(new Transaction(3, Date.valueOf("2020-05-02"), 400));
            TransactionDao.insert( new Transaction(1, Date.valueOf("2019-03-27"), "lunch", 5.5));
        }
    }

    public static void addSampleExpenseTargets() {
        if (ExpenseTargetDao.loadAll().isEmpty()) {
            ExpenseTargetDao.insert(new ExpenseTarget(1, Date.valueOf("2020-02-21"), Date.valueOf("2020-02-27"), 3, State.UNCOMPLETED));
            ExpenseTargetDao.insert(new ExpenseTarget(4, Date.valueOf("2020-01-01"), Date.valueOf("2020-12-29"), 200, State.COMPLETED));
            ExpenseTargetDao.insert(new ExpenseTarget(5, Date.valueOf("2020-03-21"), Date.valueOf("2020-04-21"), 100, State.COMPLETED));
            ExpenseTargetDao.insert(new ExpenseTarget(1, Date.valueOf("2020-05-01"), Date.valueOf("2020-06-01"), 5000, State.ACTIVE));
        }
    }
}

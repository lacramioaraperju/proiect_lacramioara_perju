package wantsome.project.db.service;

import wantsome.project.db.DbManager;
import wantsome.project.db.dto.ExpenseTarget;
import wantsome.project.db.dto.ExpenseTargetFull;
import wantsome.project.db.dto.State;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ExpenseTargetDao {

    public static List<ExpenseTargetFull> loadAll() {

        String sql = "SELECT E.*, C.NAME AS CATEGORY_NAME " +
                "FROM EXPENSE_TARGETS E " +
                "JOIN CATEGORIES C ON E.CATEGORY_ID = C.ID";

        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery(sql)) {

            List<ExpenseTargetFull> expenseTargetsList = new ArrayList<>();
            while (rs.next()) {
                expenseTargetsList.add(extractExpenseTargetFullFromResult(rs));
            }
            return expenseTargetsList;

        } catch (SQLException e) {
            throw new RuntimeException("Error loading expense targets: " + e.getMessage());
        }
    }

    private static ExpenseTargetFull extractExpenseTargetFullFromResult(ResultSet rs) throws SQLException {
        return new ExpenseTargetFull(
                rs.getInt("ID"),
                rs.getInt("CATEGORY_ID"),
                rs.getString("CATEGORY_NAME"),
                rs.getDate("START_DATE"),
                rs.getDate("END_DATE"),
                rs.getDouble("AMOUNT"),
                State.valueOf(rs.getString("STATE")));
    }

    public static Optional<ExpenseTarget> load(int id) {

        String sql = "SELECT * FROM EXPENSE_TARGETS WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(extractExpenseTargetFromResult(rs));
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading expense target " + id + ": " + e.getMessage());
        }
    }

    private static ExpenseTarget extractExpenseTargetFromResult(ResultSet rs) throws SQLException {
        return new ExpenseTarget(
                rs.getInt("ID"),
                rs.getInt("CATEGORY_ID"),
                rs.getDate("START_DATE"),
                rs.getDate("END_DATE"),
                rs.getDouble("AMOUNT"),
                State.valueOf(rs.getString("STATE")));
    }

    public static void insert(ExpenseTarget expenseTarget) {

        String sql = "INSERT INTO EXPENSE_TARGETS " +
                "(CATEGORY_ID, START_DATE, END_DATE, AMOUNT, STATE) " +
                "VALUES (?, ?, ?, ?, ?)";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, expenseTarget.getCategoryId());
            ps.setDate(2, expenseTarget.getStartDate());
            ps.setDate(3, expenseTarget.getEndDate());
            ps.setDouble(4, expenseTarget.getAmount());
            ps.setString(5, expenseTarget.getState().toString());

            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error saving new expense target: " + e.getMessage());
        }
    }

    public static void update(ExpenseTarget expenseTarget) {

        String sql = "UPDATE EXPENSE_TARGETS " +
                "SET CATEGORY_ID = ?, " +
                "START_DATE = ?, " +
                "END_DATE = ?, " +
                "AMOUNT = ?, " +
                "STATE = ?" +
                "WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, expenseTarget.getCategoryId());
            ps.setDate(2, expenseTarget.getStartDate());
            ps.setDate(3, expenseTarget.getEndDate());
            ps.setDouble(4, expenseTarget.getAmount());
            ps.setString(5, expenseTarget.getState().toString());
            ps.setInt(6, expenseTarget.getId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException("Error updating expense target " + expenseTarget.getId() + ": " + e.getMessage());
        }
    }

    public static void delete(int id) {

        String sql = "DELETE FROM EXPENSE_TARGETS WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, id);

            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error deleting expense target " + id + ": " + e.getMessage());
        }
    }

    public static void deleteAll() {
        String sql = "DELETE FROM EXPENSE_TARGETS";

        try (Connection conn = DbManager.getConnection();
             Statement ps = conn.createStatement()) {

            ps.execute(sql);

        } catch (SQLException e) {
            throw new RuntimeException("Error deleting all expense targets : " + e.getMessage());
        }
    }
}

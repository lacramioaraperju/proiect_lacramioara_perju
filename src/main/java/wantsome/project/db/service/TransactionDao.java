package wantsome.project.db.service;

import wantsome.project.db.DbManager;
import wantsome.project.db.dto.Transaction;
import wantsome.project.db.dto.TransactionFull;
import wantsome.project.db.dto.Type;

import java.sql.*;
import java.util.*;

public class TransactionDao {

    public static List<TransactionFull> loadAll() {

        String sql = "SELECT T.*, C.NAME AS CATEGORY_NAME, C.TYPE AS CATEGORY_TYPE " +
                "FROM TRANSACTIONS T " +
                "JOIN CATEGORIES C ON T.CATEGORY_ID = C.ID";

        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery(sql)) {

            List<TransactionFull> transactionsList = new ArrayList<>();
            while (rs.next()) {
                transactionsList.add(extractTransactionFullFromResult(rs));
            }
            return transactionsList;

        } catch (SQLException e) {
            throw new RuntimeException("Error loading transactions: " + e.getMessage());
        }
    }

    private static TransactionFull extractTransactionFullFromResult(ResultSet rs) throws SQLException {
        return new TransactionFull(
                rs.getInt("ID"),
                rs.getInt("CATEGORY_ID"),
                rs.getString("CATEGORY_NAME"),
                Type.valueOf(rs.getString("CATEGORY_TYPE")),
                rs.getDate("TRANSACTION_DATE"),
                rs.getString("DETAILS"),
                rs.getDouble("AMOUNT"));
    }

    public static Optional<Transaction> load(int id) {

        String sql = "SELECT * FROM TRANSACTIONS WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(extractTransactionFromResult(rs));
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading transaction " + id + ": " + e.getMessage());
        }
    }

    private static Transaction extractTransactionFromResult(ResultSet rs) throws SQLException {
        return new Transaction(
                rs.getInt("ID"),
                rs.getInt("CATEGORY_ID"),
                rs.getDate("TRANSACTION_DATE"),
                rs.getString("DETAILS"),
                rs.getDouble("AMOUNT"));
    }

    public static void insert(Transaction transaction) {

        String sql = "INSERT INTO TRANSACTIONS " +
                "(CATEGORY_ID, TRANSACTION_DATE, DETAILS, AMOUNT) " +
                "VALUES (?, ?, ?, ?)";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, transaction.getCategoryId());
            ps.setDate(2, transaction.getDate());
            ps.setString(3, transaction.getDetails());
            ps.setDouble(4, transaction.getAmount());

            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error saving new transaction: " + e.getMessage());
        }
    }

    public static void update(Transaction transaction) {

        String sql = "UPDATE TRANSACTIONS " +
                "SET CATEGORY_ID = ?, " +
                "TRANSACTION_DATE = ?, " +
                "DETAILS = ?, " +
                "AMOUNT = ? " +
                "WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, transaction.getCategoryId());
            ps.setDate(2, transaction.getDate());
            ps.setString(3, transaction.getDetails());
            ps.setDouble(4, transaction.getAmount());
            ps.setInt(5, transaction.getId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException("Error updating transaction " + transaction.getId() + ": " + e.getMessage());
        }
    }

    public static void delete(int id) {

        String sql = "DELETE FROM TRANSACTIONS WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, id);

            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException("Error deleting transaction " + id + ": " + e.getMessage());
        }
    }

    public static void deleteAll() {

        String sql = "DELETE FROM TRANSACTIONS";

        try (Connection conn = DbManager.getConnection();
             Statement ps = conn.createStatement()) {

            ps.execute(sql);
        } catch (SQLException e) {
            throw new RuntimeException("Error deleting all transactions : " + e.getMessage());
        }
    }

}

package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.Category;
import wantsome.project.db.dto.Type;
import wantsome.project.db.service.CategoryDao;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static wantsome.project.web.SparkUtil.render;

public class AddEditCategoryPageController {

    public static String showAddForm(Request request, Response response) {
        return renderAddUpdateForm(null, "", null);
    }

    public static String showEditForm(Request request, Response response) {

        String id = request.queryParams("id");
        Optional<Category> optionalCategory = CategoryDao.load(Integer.parseInt(id));

        if (optionalCategory.isEmpty()) {
            throw new RuntimeException("Category " + id + " not found!");
        }

        Category category = optionalCategory.get();
        return renderAddUpdateForm(category, id, null);
    }

    private static String renderAddUpdateForm(Category category, String id, String errorMsg) {
        Map<String, Object> model = new HashMap<>();

        model.put("oldCategory", category);
        model.put("isEdit", id != null && !id.isEmpty());
        model.put("errorMsg", errorMsg);
        model.put("typeOptions", Type.values());

        return render(model, "add_edit_category.vm");
    }

    public static String handleAddEditRequest(Request request, Response response) {

        String id = request.queryParams("id");
        String name = request.queryParams("name");
        String type = request.queryParams("type");

        Category category = validateAndBuildCategory(id, name, type);

        try {
            if (category.getId() > 0) {
                CategoryDao.update(category);
            } else {
                CategoryDao.insert(category);
            }

            response.redirect("/categories");
            return null;
        } catch (Exception e) {
            return renderAddUpdateForm(category, id, e.getMessage());
        }
    }

    private static Category validateAndBuildCategory(String id, String name, String type) {
        int idValue = id != null && !id.isEmpty() ? Integer.parseInt(id) : -1;
        Type typeValue = Type.valueOf(type);

        return new Category(idValue, name, typeValue);
    }
}

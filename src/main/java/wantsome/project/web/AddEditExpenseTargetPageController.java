package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.Category;
import wantsome.project.db.dto.ExpenseTarget;
import wantsome.project.db.dto.State;
import wantsome.project.db.service.CategoryDao;
import wantsome.project.db.service.ExpenseTargetDao;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static wantsome.project.web.SparkUtil.render;

public class AddEditExpenseTargetPageController {

    public static String showAddForm(Request request, Response response) {
        return renderAddEditForm(null, "");
    }

    public static String showEditForm(Request request, Response response) {

        String id = request.queryParams("id");
        Optional<ExpenseTarget> optionalExpenseTarget = ExpenseTargetDao.load(Integer.parseInt(id));

        if (optionalExpenseTarget.isEmpty()) {
            throw new RuntimeException("Expense target " + id + " not found!");
        }

        ExpenseTarget expenseTarget = optionalExpenseTarget.get();
        return renderAddEditForm(expenseTarget, id);
    }

    private static String renderAddEditForm(ExpenseTarget expenseTarget, String id) {
        Map<String, Object> model = new HashMap<>();

        model.put("oldExpenseTarget", expenseTarget);
        model.put("isEdit", id != null && !id.isEmpty());

        if (expenseTarget != null) {
            Optional<Category> optionalCategory = CategoryDao.load(expenseTarget.getCategoryId());
            if (optionalCategory.isEmpty()) {
                throw new RuntimeException("Category " + id + " not found!");
            }
            model.put("oldCategory", optionalCategory.get());
        }

        model.put("categories", CategoryDao.loadAll());
        model.put("stateOptions", State.values());

        return render(model, "add_edit_expenseTarget.vm");
    }

    public static String handleAddEditRequest(Request request, Response response) {
        String id = request.queryParams("id");
        String categoryId = request.queryParams("categoryId");
        String startDate = request.queryParams("startDate");
        String endDate = request.queryParams("endDate");
        String amount = request.queryParams("amount");

        ExpenseTarget expenseTarget = validateAndBuildExpenseTarget(id, categoryId, startDate, endDate, amount);

        try {
            if (expenseTarget.getId() > 0) {
                ExpenseTargetDao.update(expenseTarget);
            } else {
                ExpenseTargetDao.insert(expenseTarget);
            }

            response.redirect("/expenseTargets");
            return null;

        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private static ExpenseTarget validateAndBuildExpenseTarget(String id, String categoryId, String startDate,
                                                               String endDate, String amount) {
        int idValue = id != null && !id.isEmpty() ? Integer.parseInt(id) : -1;
        int categoryIdValue = Integer.parseInt(categoryId);
        Date startDateValue = startDate.isEmpty() ? null : Date.valueOf(startDate);
        Date endDateValue = endDate.isEmpty() ? null : Date.valueOf(endDate);
        double amountValue = Double.parseDouble(amount);
        State stateValue = State.ACTIVE;

        return new ExpenseTarget(idValue, categoryIdValue, startDateValue, endDateValue, amountValue, stateValue);
    }
}

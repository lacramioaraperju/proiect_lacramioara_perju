package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.Category;
import wantsome.project.db.dto.Transaction;
import wantsome.project.db.service.CategoryDao;
import wantsome.project.db.service.TransactionDao;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static wantsome.project.web.SparkUtil.render;

public class AddEditTransactionPageController {

    public static String showAddForm(Request request, Response response) {
        return renderAddEditForm(null, "");
    }

    public static String showEditForm(Request request, Response response) {
        String id = request.queryParams("id");
        Optional<Transaction> optionalTransaction = TransactionDao.load(Integer.parseInt(id));

        if (optionalTransaction.isEmpty()) {
            throw new RuntimeException("Transaction " + id + " not found!");
        }
        Transaction transaction = optionalTransaction.get();

        return renderAddEditForm(transaction, id);
    }


    private static String renderAddEditForm(Transaction transaction, String id) {
        Map<String, Object> model = new HashMap<>();

        model.put("oldTransaction", transaction);
        model.put("isEdit", id != null && !id.isEmpty());

        if (transaction != null) {
            Optional<Category> optionalCategory = CategoryDao.load(transaction.getCategoryId());
            if (optionalCategory.isEmpty()) {
                throw new RuntimeException("Category " + id + " not found!");
            }
            model.put("oldCategory", optionalCategory.get());
        }

        model.put("categories", CategoryDao.loadAll());

        return render(model, "add_edit_transaction.vm");
    }

    public static String handleAddEditRequest(Request request, Response response) {

        String id = request.queryParams("id");
        String categoryId = request.queryParams("categoryId");
        String date = request.queryParams("date");
        String details = request.queryParams("details");
        String amount = request.queryParams("amount");

        Transaction transaction = validateAndBuildTransaction(id, categoryId, date, details, amount);

        try {
            if (transaction.getId() > 0) {
                TransactionDao.update(transaction);
            } else {
                TransactionDao.insert(transaction);
            }

            response.redirect("/transactions");
            return null;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private static Transaction validateAndBuildTransaction(String id, String categoryId, String date, String details, String amount) {
        int idValue = id != null && !id.isEmpty() ? Integer.parseInt(id) : -1;
        int categoryIdValue = Integer.parseInt(categoryId);
        Date dateValue = date.isEmpty() ? null : Date.valueOf(date);
        double amountValue = Double.parseDouble(amount);

        return new Transaction(idValue, categoryIdValue, dateValue, details, amountValue);
    }

}

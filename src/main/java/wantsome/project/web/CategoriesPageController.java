package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.Category;
import wantsome.project.db.dto.Type;
import wantsome.project.db.service.CategoryDao;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static wantsome.project.web.SparkUtil.render;

public class CategoriesPageController {

    public enum Order {

        ID_ASC ("Ascending Id"),
        ID_DESC ("Descending Id");

        private final String label;

        Order(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }

    public static String showCategoriesPage(Request request, Response response) {
        return renderCategoriesPage(request);
    }

    private static String renderCategoriesPage(Request request) {

        Type filter = getFilterFromQueryOrSession(request);
        Order order = getOrderFromQueryOrSession(request);

        List<Category> categoryList = getCategoriesToDisplay(CategoryDao.loadAll(), filter, order);

        Map<String, Object> model = new HashMap<>();

        model.put("categories", categoryList);
        model.put("currentFilter", filter);

        model.put("orderOption", Order.values());
        model.put("currentOrder", order);

        return render(model, "categories.vm");
    }

    private static Type getFilterFromQueryOrSession(Request request) {
        String param = fromQueryOrSession(request, "categoriesFilter");
        return ("ALL".equals(param)) || param == null ?
                null :
                Type.valueOf(param);
    }

    private static Order getOrderFromQueryOrSession(Request response) {
        String param = fromQueryOrSession(response, "categoriesOrder");
        return param == null ?
                Order.ID_ASC :
                Order.valueOf(param);
    }


    private static String fromQueryOrSession(Request request, String name) {
        String param = request.queryParams(name);
        if (param != null) {
            request.session().attribute(name, param);
        } else {
            param = request.session().attribute(name);
        }
        return param;
    }

    private static List<Category> getCategoriesToDisplay(List<Category> allCategories, Type filter, Order order) {
        return allCategories.stream()
                .filter(c -> filter == null || c.getType() == filter)
                .sorted(getDisplayComparator(order))
                .collect(Collectors.toList());
    }

    private static Comparator<Category> getDisplayComparator(Order order) {
        if (order == Order.ID_ASC) {
            return Comparator.comparingInt(Category::getId);
        } else {
            return  (i1, i2) -> Integer.compare(i2.getId(), i1.getId());
        }
    }

    public static String handleDeleteRequest(Request request, Response response) {
        String id = request.queryParams("id");
        CategoryDao.delete(Integer.parseInt(id));
        response.redirect("/categories");
        return null;
    }

    public static String handleDeleteAllRequest(Request request, Response response) {
        CategoryDao.deleteAll();
        response.redirect("/categories");
        return null;
    }
}

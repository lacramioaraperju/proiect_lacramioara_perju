package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.*;
import wantsome.project.db.service.CategoryDao;
import wantsome.project.db.service.ExpenseTargetDao;
import wantsome.project.db.service.TransactionDao;

import java.sql.Date;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static wantsome.project.web.SparkUtil.render;

public class ExpenseTargetsPageController {

    public enum Filter{
        ALL, CATEGORY, STATE
    }

    public static String showExpenseTargetsPage(Request request, Response response) {
        return renderExpenseTargetsPage(request);
    }

    private static String renderExpenseTargetsPage(Request request) {
        Map<String, Object> model = new HashMap<>();

        Filter filter = getFilterFromQueryOrSes(request);
        Order order = getOrderFromQueryOrSes(request);
        String param = getExtraParam(request, filter);

        List<ExpenseTarget> expenseTargetsChange = checkExpenseTargets();
        model.put("expenseTargetsChange", expenseTargetsChange);

        List<ExpenseTargetFull> expenseTargetList = getExpenseTargetsToDisplay(request, ExpenseTargetDao.loadAll(), filter, order, param);
        model.put("expenseTargets", expenseTargetList);

        model.put("currentFilter", filter);
        model.put("categoryOptions", CategoryDao.loadAll());
        model.put("oldCategory", param);

        model.put("stateOptions", State.values());
        model.put("oldState", param);

        model.put("orderOption", Order.values());
        model.put("currentOrder", order);

        model.put("stateCount", getStatesCount());

        return render(model, "expenseTargets.vm");
    }

    private static Filter getFilterFromQueryOrSes(Request request) {
        String param = fromQueryOrSession(request, "expenseTargetsFilter");
        return param == null ?
                Filter.ALL :
                Filter.valueOf(param);
    }

    private static Order getOrderFromQueryOrSes(Request request) {
        String param = fromQueryOrSession(request, "expenseTargetsOrder");
        return param == null ?
                Order.ID_ASC :
                Order.valueOf(param);
    }

    private static String fromQueryOrSession(Request request, String name) {
        String param = request.queryParams(name);
        if (param != null) {
            request.session().attribute(name, param);
        } else {
            param = request.session().attribute(name);
        }
        return param;
    }

    private static List<ExpenseTargetFull> getExpenseTargetsToDisplay(Request request, List<ExpenseTargetFull> allExpenseTargets,
                                                                      Filter filter, Order order, String param) {
        return allExpenseTargets.stream()
                .filter(i -> getDisplayFilter(i, filter, param))
                .sorted(getDisplayComparator(order))
                .collect(Collectors.toList());
    }

    private static String getExtraParam(Request request, Filter filter) {
        if (filter == Filter.CATEGORY) {
            return getParamFromQueryOrSession(request, "expenseTargetsFilterCategoryId", getDefaultId());
        } else if (filter == Filter.STATE) {
            return getParamFromQueryOrSession(request, "expenseTargetsFilterState", State.ACTIVE.toString());
        }
        return null;
    }

    private static String getParamFromQueryOrSession(Request request, String name, String defaultValue) {
        String param = fromQueryOrSession(request, name);
        return  param == null ?
                defaultValue :
                param;
    }

    private static String getDefaultId() {
        return String.valueOf(CategoryDao.loadAll().stream()
                .filter(i -> i.getType() == Type.EXPENSE)
                .map(Category::getId)
                .findFirst()
                .orElse(1));
    }

    private static boolean getDisplayFilter(ExpenseTargetFull i, Filter filter, String param1) {
        if (filter == Filter.ALL) {
            return true;
        } else if (filter == Filter.CATEGORY) {
            return i.getCategoryId() == Integer.parseInt(param1);
        } else {
            return i.getState() == State.valueOf(param1);
        }
    }

    private static Comparator<ExpenseTargetFull> getDisplayComparator(Order order) {
        if (order == Order.ID_ASC) {
            return Comparator.comparingInt(ExpenseTargetFull::getId);
        } else if (order == Order.ID_DESC) {
            return  (i1, i2) -> Integer.compare(i2.getId(), i1.getId());
        } else if (order == Order.DATE_ASC) {
            return ExpenseTargetsPageController::compareDate;
        } else if (order == Order.DATE_DESC) {
            return (i1, i2) -> compareDate(i2, i1);
        } else if (order == Order.AMOUNT_ASC) {
            return Comparator.comparingDouble(ExpenseTargetFull::getAmount);
        } else {
            return (i1, i2) -> Double.compare(i2.getAmount(), i1.getAmount());
        }
    }

    private static int compareDate(ExpenseTargetFull i1, ExpenseTargetFull i2) {
        Date defaultDate = new Date(0);
        Date d1 = i1.getStartDate() != null ? i1.getStartDate() : defaultDate;
        Date d2 = i2.getStartDate() != null ? i2.getStartDate() : defaultDate;
        return d1.compareTo(d2);
    }

    public static String handleDeleteRequest(Request request, Response response) {
        String id = request.queryParams("id");
        ExpenseTargetDao.delete(Integer.parseInt(id));
        response.redirect("/expenseTargets");
        return null;
    }

    public static String handleDeleteAllRequest(Request request, Response response) {
        ExpenseTargetDao.deleteAll();
        response.redirect("/expenseTargets");
        return null;
    }

    private static Map<String, Long> getStatesCount() {
        return ExpenseTargetDao.loadAll().stream()
                .collect(groupingBy(i -> i.getState().getLabel(), counting()));
    }

    public static List<ExpenseTarget> checkExpenseTargets() {
        return getExpenseTargetsActive().stream()
                .map(ExpenseTargetsPageController::checkIfStatusNeedUpdate)
                .collect(Collectors.toList());
    }

    private static ExpenseTarget checkIfStatusNeedUpdate(ExpenseTargetFull expenseTarget) {

        Date now = Date.valueOf(LocalDate.now());
        double sumAmount = getSumAmountTransactions(expenseTarget.getStartDate(), expenseTarget.getEndDate(), expenseTarget.getCategoryId());

        if (expenseTarget.getAmount() < sumAmount) {
            return updateStatus(expenseTarget, State.UNCOMPLETED);
        } else {
            if (expenseTarget.getEndDate().before(now)) {
                return updateStatus(expenseTarget, State.COMPLETED);
            }
        }
        return null;
    }

    private static ExpenseTarget updateStatus(ExpenseTargetFull expenseTarget, State state) {

        ExpenseTargetDao.update(new ExpenseTarget(expenseTarget.getId(), expenseTarget.getCategoryId(),
                expenseTarget.getStartDate(), expenseTarget.getEndDate(), expenseTarget.getAmount(), state));

        Optional<ExpenseTarget> target = ExpenseTargetDao.load(expenseTarget.getId());
        if (target.isEmpty()) {
            throw new RuntimeException("Expense target " + expenseTarget.getId() + " not found!");
        }
        return target.get();
    }

    private static double getSumAmountTransactions(Date startDate, Date endDate, int categoryId) {
        return TransactionDao.loadAll().stream()
                .filter(i -> i.getCategoryId() == categoryId)
                .filter(i -> !i.getDate().before(startDate) && !i.getDate().after(endDate))
                .mapToDouble(TransactionFull::getAmount)
                .sum();
    }

    private static List<ExpenseTargetFull> getExpenseTargetsActive() {
        return ExpenseTargetDao.loadAll().stream()
                .filter(i -> i.getState() == State.ACTIVE)
                .collect(Collectors.toList());
    }
}

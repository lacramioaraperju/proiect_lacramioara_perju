package wantsome.project.web;

public enum Order {

    ID_ASC ("Ascending Id"),
    ID_DESC ("Descending Id"),
    DATE_ASC ("Ascending Date"),
    DATE_DESC ("Descending Date"),
    AMOUNT_ASC ("Ascending Amount"),
    AMOUNT_DESC ("Descending Amount");

    private final String label;

    Order(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}

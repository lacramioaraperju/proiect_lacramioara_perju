package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.TransactionFull;
import wantsome.project.db.dto.Type;
import wantsome.project.db.service.TransactionDao;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingDouble;
import static wantsome.project.web.SparkUtil.render;

public class ReportsPageController {


    public static String showReportsPage(Request request, Response response) {
        return renderReportsPage(request);
    }

    private static String renderReportsPage(Request request) {
        Map<String, Object> model = new HashMap<>();

        Date startDate = getStartDateFromQueryOrSes(request);
        Date endDate = getEndDateFromQueryOrSes(request);

        Map<String, Double> sumForType = getSumForTypes(startDate, endDate);
        sumForType.putIfAbsent("Expense", 0.0);
        sumForType.putIfAbsent("Income", 0.0);

        double sumExpenses = sumForType.get("Expense");
        double sumIncomes = sumForType.get("Income");

        double balance = sumIncomes - sumExpenses;

        model.put("balance", balance);
        model.put("sumExpenses", sumExpenses);
        model.put("sumIncomes", sumIncomes);

        model.put("sumPerCategoryExpenses", getSumPerCategory(startDate, endDate, Type.EXPENSE));
        model.put("sumPerCategoryIncomes", getSumPerCategory(startDate, endDate, Type.INCOME));

        model.put("sumForTypes", getSumForTypes(startDate, endDate));

        model.put("oldStartDate", startDate);
        model.put("oldEndDate", endDate);

        return render(model, "reports.vm");
    }

    private static Map<String, Double> getSumPerCategory(Date startDate, Date endDate, Type type) {
        return TransactionDao.loadAll().stream()
                .filter(i -> i.getCategoryType() == type)
                .filter(i -> !i.getDate().before(startDate) && !i.getDate().after(endDate))
                .collect(groupingBy(TransactionFull::getCategoryName, summingDouble(TransactionFull::getAmount)));
    }

    private static Map<String, Double> getSumForTypes(Date startDate, Date endDate) {
        return TransactionDao.loadAll().stream()
                .filter(i -> !i.getDate().before(startDate) && !i.getDate().after(endDate))
                .collect(groupingBy(i -> i.getCategoryType().getLabel(), summingDouble(TransactionFull::getAmount)));
    }

    private static Date getStartDateFromQueryOrSes(Request request) {
        String param = fromQueryOrSession(request, "startDate");
        return param == null ?
                getDefaultStartDate() :
                Date.valueOf(param);
    }

    private static Date getDefaultStartDate() {
        return TransactionDao.loadAll().stream()
                .sorted(ReportsPageController::compareDate)
                .map(TransactionFull::getDate)
                .findFirst()
                .orElse(new Date(0));
    }

    private static Date getEndDateFromQueryOrSes(Request request) {
        String param = fromQueryOrSession(request, "endDate");
        return param == null ?
                getDefaultEndDate() :
                Date.valueOf(param);
    }

    private static Date getDefaultEndDate() {
        return TransactionDao.loadAll().stream()
                .sorted((i1, i2) -> compareDate(i2, i1))
                .map(TransactionFull::getDate)
                .findFirst()
                .orElse(new Date(0));
    }

    private static int compareDate(TransactionFull i1, TransactionFull i2) {
        Date defaultDate = new Date(0);
        Date d1 = i1.getDate() != null ? i1.getDate() : defaultDate;
        Date d2 = i2.getDate() != null ? i2.getDate() : defaultDate;
        return d1.compareTo(d2);
    }

    private static String fromQueryOrSession(Request request, String name) {
        String param = request.queryParams(name);
        if (param != null) {
            request.session().attribute(name, param);
        } else {
            param = request.session().attribute(name);
        }
        return param;
    }
}

package wantsome.project.web;

import spark.Request;
import spark.Response;
import wantsome.project.db.dto.*;
import wantsome.project.db.service.CategoryDao;
import wantsome.project.db.service.TransactionDao;

import java.sql.Date;
import java.util.*;
import java.util.stream.Collectors;

import static wantsome.project.web.SparkUtil.render;

public class TransactionsPageController {

    public enum Filter {
        ALL, CATEGORY, CATEGORY_TYPE
    }

    public static String showTransactionsPage(Request request, Response response) {
        return renderTransactionsPage(request);
    }

    private static String renderTransactionsPage(Request request) {
        Map<String, Object> model = new HashMap<>();

        Filter filter = getFilterFromQueryOrSession(request);
        Order order = getOrderFromQueryOrSession(request);
        String param = getExtraParam(request, filter);

        List<ExpenseTarget> expenseTargetsChange = ExpenseTargetsPageController.checkExpenseTargets();
        model.put("expenseTargetsChange", expenseTargetsChange);

        List<TransactionFull> transactionList = getTransactionsToDisplay(request, TransactionDao.loadAll(), filter, order, param);

        model.put("transactions", transactionList);

        model.put("currentFilter", filter);
        model.put("categoryOptions", CategoryDao.loadAll());
        model.put("oldCategory", param);
        model.put("typeOptions", Type.values());
        model.put("oldType", param);

        model.put("orderOption", Order.values());
        model.put("currentOrder", order);

        return render(model, "transactions.vm");
    }

    private static Filter getFilterFromQueryOrSession(Request request) {
        String param = fromQueryOrSession(request, "transactionsFilter");
        return param == null ?
                Filter.ALL :
                Filter.valueOf(param);
    }

    private static Order getOrderFromQueryOrSession(Request request) {
        String param = fromQueryOrSession(request, "transactionsOrder");
        return param == null ?
                Order.ID_ASC :
                Order.valueOf(param);
    }

    private static String fromQueryOrSession(Request request, String name) {
        String param = request.queryParams(name);
        if (param != null) {
            request.session().attribute(name, param);
        } else {
            param = request.session().attribute(name);
        }
        return param;
    }

    private static List<TransactionFull> getTransactionsToDisplay(Request request, List<TransactionFull> allTransactions,
                                                                  Filter filter, Order order, String param) {
        return allTransactions.stream()
                .filter(i -> getDisplayFilter(i, filter, param))
                .sorted(getDisplayComparator(order))
                .collect(Collectors.toList());
    }

    private static String getExtraParam(Request request, Filter filter) {
        if (filter == Filter.CATEGORY) {
            return getParamFromQueryOrSession(request, "transactionsFilterCategoryId", "1");
        } else if (filter == Filter.CATEGORY_TYPE) {
            return getParamFromQueryOrSession(request, "transactionsFilterCategoryType", "EXPENSE");
        }
        return null;
    }

    private static String getParamFromQueryOrSession(Request request, String name, String defaultValue) {
        String param = fromQueryOrSession(request, name);
        return  param == null ?
                defaultValue :
                param;
    }

    private static boolean getDisplayFilter(TransactionFull i, Filter filter, String param1) {
        if (filter == Filter.ALL) {
            return true;
        } else if (filter == Filter.CATEGORY) {
            return i.getCategoryId() == Integer.parseInt(param1);
        } else {
            return i.getCategoryType() == Type.valueOf(param1);
        }
    }

    private static Comparator<TransactionFull> getDisplayComparator(Order order) {
        if (order == Order.ID_ASC) {
            return Comparator.comparingInt(TransactionFull::getId);
        } else if (order == Order.ID_DESC) {
            return  (i1, i2) -> Integer.compare(i2.getId(), i1.getId());
        } else if (order == Order.DATE_ASC) {
            return TransactionsPageController::compareDate;
        } else if (order == Order.DATE_DESC) {
            return (i1, i2) -> compareDate(i2, i1);
        } else if (order == Order.AMOUNT_ASC) {
            return Comparator.comparingDouble(TransactionFull::getAmount);
        } else {
            return (i1, i2) -> Double.compare(i2.getAmount(), i1.getAmount());
        }
    }

    private static int compareDate(TransactionFull i1, TransactionFull i2) {
        Date defaultDate = new Date(0);
        Date d1 = i1.getDate() != null ? i1.getDate() : defaultDate;
        Date d2 = i2.getDate() != null ? i2.getDate() : defaultDate;
        return d1.compareTo(d2);
    }

    public static String handleDeleteRequest(Request request, Response response) {
        String id = request.queryParams("id");
        TransactionDao.delete(Integer.parseInt(id));
        response.redirect("/transactions");
        return null;
    }

    public static String handleDeleteAllRequest(Request request, Response response) {
        TransactionDao.deleteAll();
        response.redirect("/transactions");
        return null;
    }
}

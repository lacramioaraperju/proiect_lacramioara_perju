package wantsome.project.db.service;

import org.junit.*;
import wantsome.project.db.DbManager;
import wantsome.project.db.dto.Category;
import wantsome.project.db.dto.Type;

import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

public class CategoryDaoTest {

    private static final String TEST_DB_FILE = "budget_tracker_test.db";

    private static final List<Category> sampleCategories = List.of(
            new Category("food", Type.EXPENSE),
            new Category("salary", Type.INCOME),
            new Category("rent", Type.EXPENSE),
            new Category("other", Type.INCOME));


    @BeforeClass
    public static void initDbBeforeAnyTests() {
        DbManager.setDbFile(TEST_DB_FILE);
        DbInitService.initDatabase();
        CategoryDao.deleteAll();
    }

    @Before
    public void insertRowsBeforeTest() {
        assertTrue(CategoryDao.loadAll().isEmpty());
        for (Category item : sampleCategories) {
            CategoryDao.insert(item);
        }
    }

    @After
    public void deleteRowsAfterTest() {
        for (Category item : CategoryDao.loadAll()) {
            CategoryDao.delete(item.getId());
        }
        assertTrue(CategoryDao.loadAll().isEmpty());
    }

    @AfterClass
    public static void deleteDbFileAfterAllTests() {
        new File(TEST_DB_FILE).delete();
    }

    @Test
    public void insert() {
        assertEquals(4, CategoryDao.loadAll().size());

        Category newCategory = new Category("other", Type.EXPENSE);
        CategoryDao.insert(newCategory);

        assertEquals(5, CategoryDao.loadAll().size());
    }

    @Test
    public void getAll() {
        checkOnlyTheSampleItemsArePresentInDb();
    }

    private void checkOnlyTheSampleItemsArePresentInDb() {

        List<Category> itemsFromDb = CategoryDao.loadAll();
        assertEquals(4, itemsFromDb.size());
        assertEqualItemsExceptId(itemsFromDb.get(0), sampleCategories.get(0));
        assertEqualItemsExceptId(itemsFromDb.get(1), sampleCategories.get(1));
        assertEqualItemsExceptId(itemsFromDb.get(2), sampleCategories.get(2));
        assertEqualItemsExceptId(itemsFromDb.get(3), sampleCategories.get(3));
    }

    private void assertEqualItemsExceptId(Category item1, Category item2) {
        assertTrue("items should be equal (except id): " + item1 + ", " + item2,
                item1.getName().equals(item2.getName()) &&
                        item1.getType() == item2.getType());
    }

    @Test
    public void get() {
        Category item1fromDb = CategoryDao.loadAll().get(0);

        assertEquals(item1fromDb, CategoryDao.load(item1fromDb.getId()).get());
    }

    @Test
    public void get_forInvalidId() {
        assertFalse(CategoryDao.load(-99).isPresent());
    }

    @Test
    public void delete() {
        List<Category> itemsFromDb = CategoryDao.loadAll();
        CategoryDao.delete(itemsFromDb.get(0).getId());

        List<Category> itemsInDbAfterDelete = CategoryDao.loadAll();
        assertEquals(3, itemsInDbAfterDelete.size());
        assertEqualItemsExceptId(itemsFromDb.get(1), itemsInDbAfterDelete.get(0));
        assertEqualItemsExceptId(itemsFromDb.get(2), itemsInDbAfterDelete.get(1));
        assertEqualItemsExceptId(itemsFromDb.get(3), itemsInDbAfterDelete.get(2));
    }

    @Test
    public void delete_forInvalidId() {
        checkOnlyTheSampleItemsArePresentInDb();

        CategoryDao.delete(-2);

        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void deleteAll() {
        assertEquals(4, CategoryDao.loadAll().size());

        CategoryDao.deleteAll();

        assertTrue(CategoryDao.loadAll().isEmpty());
    }

    @Test
    public void update() {
        Category item1fromDb = CategoryDao.loadAll().get(0);
        Category updatedItem = new Category(item1fromDb.getId(), "other", item1fromDb.getType());
        CategoryDao.update(updatedItem);
        assertEquals(updatedItem, CategoryDao.loadAll().get(0));
    }

    @Test
    public void update_forInvalidId() {
        Category item1fromDb = CategoryDao.loadAll().get(0);
        Category updatedItem = new Category(-24, "other", item1fromDb.getType());
        CategoryDao.update(updatedItem);

        checkOnlyTheSampleItemsArePresentInDb();
    }
}

package wantsome.project.db.service;

import org.junit.*;
import wantsome.project.db.DbManager;
import wantsome.project.db.dto.ExpenseTarget;
import wantsome.project.db.dto.ExpenseTargetFull;
import wantsome.project.db.dto.State;

import java.io.File;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class ExpenseTargetDaoTest {

    private static final String TEST_DB_FILE = "budget_tracker_test.db";
    private static final int DEFAULT_CATEGORY_ID = 1;
    private static final long baseTime = System.currentTimeMillis();

    private static final List<ExpenseTarget> sampleExpensesTarget = List.of(
            new ExpenseTarget(DEFAULT_CATEGORY_ID, new Date(baseTime), new Date(baseTime + 2000), 3, State.UNCOMPLETED),
            new ExpenseTarget(DEFAULT_CATEGORY_ID, new Date(baseTime - 1000), new Date(baseTime + 3000), 200, State.COMPLETED),
            new ExpenseTarget(DEFAULT_CATEGORY_ID, new Date(baseTime + 1000), new Date(baseTime + 2000), 100, State.COMPLETED),
            new ExpenseTarget(DEFAULT_CATEGORY_ID, new Date(baseTime + 1000), new Date(baseTime + 3000), 5000, State.ACTIVE));


    @BeforeClass
    public static void initDbBeforeAnyTests() {
        DbManager.setDbFile(TEST_DB_FILE);
        DbInitService.initDatabase();
    }

    @Before
    public void insertRowsBeforeTest() {
        assertTrue(ExpenseTargetDao.loadAll().isEmpty());
        for (ExpenseTarget item : sampleExpensesTarget) {
            ExpenseTargetDao.insert(item);
        }
    }

    @After
    public void deleteRowsAfterTest() {
        ExpenseTargetDao.loadAll().forEach(dto -> ExpenseTargetDao.delete(dto.getId()));
        assertTrue(ExpenseTargetDao.loadAll().isEmpty());
    }

    @AfterClass
    public static void deleteDbFileAfterAllTests() {
        new File(TEST_DB_FILE).delete();
    }

    @Test
    public void insert() {
        assertEquals(4, ExpenseTargetDao.loadAll().size());

        ExpenseTarget newExpenseTarget = new ExpenseTarget(1, new Date(baseTime), new Date(baseTime + 1000), 50, State.ACTIVE);
        ExpenseTargetDao.insert(newExpenseTarget);

        assertEquals(5, ExpenseTargetDao.loadAll().size());
    }

    @Test
    public void getAll() {
        checkOnlyTheSampleItemsArePresentInDb();
    }

    private void checkOnlyTheSampleItemsArePresentInDb() {

        List<ExpenseTargetFull> itemsFromDb = ExpenseTargetDao.loadAll();
        assertEquals(4, itemsFromDb.size());
        assertEqualItemsExceptId(sampleExpensesTarget.get(0), itemsFromDb.get(0));
        assertEqualItemsExceptId(sampleExpensesTarget.get(1), itemsFromDb.get(1));
        assertEqualItemsExceptId(sampleExpensesTarget.get(2), itemsFromDb.get(2));
        assertEqualItemsExceptId(sampleExpensesTarget.get(3), itemsFromDb.get(3));
    }

    private void assertEqualItemsExceptId(ExpenseTargetFull item1, ExpenseTargetFull item2) {
        assertTrue("items should be equal (except id): " + item1 + ", " + item2,
                item1.getCategoryId() == item2.getCategoryId() &&
                        item1.getCategoryName().equals(item2.getCategoryName()) &&
                        item1.getStartDate().equals(item2.getStartDate()) &&
                        item1.getEndDate().equals(item2.getEndDate()) &&
                        item1.getAmount() == item2.getAmount() &&
                        item1.getState() == item2.getState());
    }

    private void assertEqualItemsExceptId(ExpenseTarget item1, ExpenseTargetFull item2) {
        assertTrue("items should be equal (except id): " + item1 + ", " + item2,
                item1.getCategoryId() == item2.getCategoryId() &&
                        item1.getStartDate().equals(item2.getStartDate()) &&
                        item1.getEndDate().equals(item2.getEndDate()) &&
                        item1.getAmount() == item2.getAmount() &&
                        item1.getState() == item2.getState());
    }

    private void assertEqualItemsExceptId(ExpenseTarget item1, ExpenseTarget item2) {
        assertTrue("items should be equal (except id): " + item1 + ", " + item2,
                item1.getCategoryId() == item2.getCategoryId() &&
                        item1.getStartDate().equals(item2.getStartDate()) &&
                        item1.getEndDate().equals(item2.getEndDate()) &&
                        item1.getAmount() == item2.getAmount() &&
                        item1.getState() == item2.getState());
    }

    @Test
    public void get() {
        ExpenseTargetFull item1fromDb = ExpenseTargetDao.loadAll().get(0);

        Optional<ExpenseTarget> optionalExpenseTarget = ExpenseTargetDao.load(item1fromDb.getId());
        assertTrue(optionalExpenseTarget.isPresent());

        assertEqualItemsExceptId(sampleExpensesTarget.get(0), optionalExpenseTarget.get());
    }

    @Test
    public void get_forInvalidId() {
        assertFalse(ExpenseTargetDao.load(-99).isPresent());
    }

    @Test
    public void update() {

        ExpenseTargetFull item1fromDb = ExpenseTargetDao.loadAll().get(0);
        ExpenseTarget updatedItem = new ExpenseTarget(item1fromDb.getId(),
                item1fromDb.getCategoryId(), item1fromDb.getStartDate(), item1fromDb.getEndDate(), 100, item1fromDb.getState());

        ExpenseTargetDao.update(updatedItem);
        assertEqualItemsExceptId(updatedItem, ExpenseTargetDao.loadAll().get(0));
    }

    @Test
    public void update_forInvalidId() {
        ExpenseTargetFull item1fromDb = ExpenseTargetDao.loadAll().get(0);
        ExpenseTarget updatedItem = new ExpenseTarget(-24,
                item1fromDb.getCategoryId(), item1fromDb.getStartDate(), item1fromDb.getEndDate(), 100, item1fromDb.getState());

        ExpenseTargetDao.update(updatedItem);

        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void delete() {
        List<ExpenseTargetFull> itemsFromDb = ExpenseTargetDao.loadAll();
        ExpenseTargetDao.delete(itemsFromDb.get(0).getId());

        List<ExpenseTargetFull> itemsInDbAfterDelete = ExpenseTargetDao.loadAll();
        assertEquals(3, itemsInDbAfterDelete.size());
        assertEqualItemsExceptId(itemsFromDb.get(1), itemsInDbAfterDelete.get(0));
        assertEqualItemsExceptId(itemsFromDb.get(2), itemsInDbAfterDelete.get(1));
        assertEqualItemsExceptId(itemsFromDb.get(3), itemsInDbAfterDelete.get(2));
    }

    @Test
    public void delete_forInvalidId() {
        checkOnlyTheSampleItemsArePresentInDb();

        ExpenseTargetDao.delete(-2);

        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void deleteAll() {
        assertEquals(4, ExpenseTargetDao.loadAll().size());

        ExpenseTargetDao.deleteAll();

        assertTrue(ExpenseTargetDao.loadAll().isEmpty());
    }
}

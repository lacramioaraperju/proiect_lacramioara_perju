package wantsome.project.db.service;

import org.junit.*;
import wantsome.project.db.DbManager;
import wantsome.project.db.dto.Transaction;
import wantsome.project.db.dto.TransactionFull;

import java.io.File;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class TransactionDaoTest {

    private static final String TEST_DB_FILE = "budget_tracker_test.db";
    private static final int DEFAULT_CATEGORY_ID = 1;
    private static final long baseTime = System.currentTimeMillis();

    private static final List<Transaction> sampleTransactions = List.of(
            new Transaction(DEFAULT_CATEGORY_ID, new Date(baseTime), "dinner", 12.5),
            new Transaction(DEFAULT_CATEGORY_ID, new Date(baseTime + 1000), "water", 40.8),
            new Transaction(DEFAULT_CATEGORY_ID, new Date(baseTime + 1000), "salary", 2000),
            new Transaction(DEFAULT_CATEGORY_ID, new Date(baseTime - 1000), 400),
            new Transaction(DEFAULT_CATEGORY_ID, new Date(baseTime - 2000), "lunch", 5.5));

    @BeforeClass
    public static void initDbBeforeAnyTests() {
        DbManager.setDbFile(TEST_DB_FILE);
        DbInitService.initDatabase();
    }

    @Before
    public void insertRowsBeforeTest() {
        assertTrue(TransactionDao.loadAll().isEmpty());
        for (Transaction item : sampleTransactions) {
            TransactionDao.insert(item);
        }
    }

    @After
    public void deleteRowsAfterTest() {
        TransactionDao.loadAll().forEach(dto -> TransactionDao.delete(dto.getId()));
        assertTrue(TransactionDao.loadAll().isEmpty());
    }

    @AfterClass
    public static void deleteDbFileAfterAllTests() {
        new File(TEST_DB_FILE).delete();
    }

    @Test
    public void insert() {
        assertEquals(5, TransactionDao.loadAll().size());

        Transaction newTransaction = new Transaction(1, new Date(baseTime + 1000), "dinner", 15.5);
        TransactionDao.insert(newTransaction);

        assertEquals(6, TransactionDao.loadAll().size());
    }

    @Test
    public void getAll() {
        checkOnlyTheSampleItemsArePresentInDb();
    }

    private void checkOnlyTheSampleItemsArePresentInDb() {

        List<TransactionFull> itemsFromDb = TransactionDao.loadAll();
        assertEquals(5, itemsFromDb.size());
        assertEqualItemsExceptId(sampleTransactions.get(0), itemsFromDb.get(0));
        assertEqualItemsExceptId(sampleTransactions.get(1), itemsFromDb.get(1));
        assertEqualItemsExceptId(sampleTransactions.get(2), itemsFromDb.get(2));
        assertEqualItemsExceptId(sampleTransactions.get(3), itemsFromDb.get(3));
        assertEqualItemsExceptId(sampleTransactions.get(4), itemsFromDb.get(4));
    }

    private void assertEqualItemsExceptId(TransactionFull item1, TransactionFull item2) {
        assertTrue("items should be equal (except id): " + item1 + ", " + item2,
                item1.getCategoryId() == item2.getCategoryId() &&
                        item1.getCategoryName().equals(item2.getCategoryName()) &&
                        item1.getCategoryType().equals(item2.getCategoryType()) &&
                        item1.getDate().equals(item2.getDate()) &&
                        item1.getDetails().equals(item2.getDetails()) &&
                        item1.getAmount() == item2.getAmount());
    }

    private void assertEqualItemsExceptId(Transaction item1, TransactionFull item2) {
        assertTrue("items should be equal (except id): " + item1 + ", " + item2,
                item1.getCategoryId() == item2.getCategoryId() &&
                        item1.getDate().equals(item2.getDate()) &&
                        item1.getDetails().equals(item2.getDetails()) &&
                        item1.getAmount() == item2.getAmount());
    }

    private void assertEqualItemsExceptId(Transaction item1, Transaction item2) {
        assertTrue("items should be equal (except id): " + item1 + ", " + item2,
                item1.getCategoryId() == item2.getCategoryId() &&
                        item1.getDate().equals(item2.getDate()) &&
                        item1.getDetails().equals(item2.getDetails()) &&
                        item1.getAmount() == item2.getAmount());
    }

    @Test
    public void get() {
        TransactionFull item1fromDb = TransactionDao.loadAll().get(0);

        Optional<Transaction> optionalTransaction = TransactionDao.load(item1fromDb.getId());
        assertTrue(optionalTransaction.isPresent());

        assertEqualItemsExceptId(sampleTransactions.get(0), optionalTransaction.get());
    }

    @Test
    public void get_forInvalidId() {
        assertFalse(TransactionDao.load(-99).isPresent());
    }

    @Test
    public void update() {
        TransactionFull item1fromDb = TransactionDao.loadAll().get(0);
        Transaction updatedItem = new Transaction(item1fromDb.getId(), item1fromDb.getCategoryId(), item1fromDb.getDate(), item1fromDb.getDetails(), 22.5);

        TransactionDao.update(updatedItem);
        assertEqualItemsExceptId(updatedItem, TransactionDao.loadAll().get(0));
    }

    @Test
    public void update_forInvalidId() {
        TransactionFull item1fromDb = TransactionDao.loadAll().get(0);
        Transaction updatedItem = new Transaction(-24, item1fromDb.getCategoryId(), item1fromDb.getDate(), item1fromDb.getDetails(), 22.5);
        TransactionDao.update(updatedItem);

        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void delete() {
        List<TransactionFull> itemsFromDb = TransactionDao.loadAll();
        TransactionDao.delete(itemsFromDb.get(0).getId());

        List<TransactionFull> itemsInDbAfterDelete = TransactionDao.loadAll();
        assertEquals(4, itemsInDbAfterDelete.size());
        assertEqualItemsExceptId(itemsFromDb.get(1), itemsInDbAfterDelete.get(0));
        assertEqualItemsExceptId(itemsFromDb.get(2), itemsInDbAfterDelete.get(1));
        assertEqualItemsExceptId(itemsFromDb.get(3), itemsInDbAfterDelete.get(2));
        assertEqualItemsExceptId(itemsFromDb.get(4), itemsInDbAfterDelete.get(3));
    }

    @Test
    public void delete_forInvalidId() {
        checkOnlyTheSampleItemsArePresentInDb();

        TransactionDao.delete(-2);

        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void deleteAll() {
        assertEquals(5, TransactionDao.loadAll().size());

        TransactionDao.deleteAll();

        assertTrue(TransactionDao.loadAll().isEmpty());
    }
}
